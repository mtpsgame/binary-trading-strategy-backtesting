import json
import os
import sys

import project_root


class ParamsReader(object):
    """
    Provides access to the values inside the params.json file.
    """

    def __init__(self):
        """
        Initialisation.
        """
        self._config_file = None
        self._content = None
        self.reload()

    def reload(self):
        """
        Reloads the json file in memory.
        """
        self._config_file = open(os.path.join(project_root.root_directory, "params", "params.json"), 'r')
        self._content = json.load(self._config_file)

    def get_value(self, *key_path):
        """
        Gets the value from the params.json file associated with the specified key path.
        Assumes there are no JSON arrays.
        :param key_path: List of json keys.
        :return: Value found at key path, or None if the key path was invalid.
        """
        if len(key_path) > 0:
            value = self._content
            try:
                for key in key_path:
                    value = value[key]
            except KeyError:
                print("JSON Error: Invalid key path " + str(key_path), file=sys.stderr)
                return None
            return value
        else:
            print("JSON Error: No key path provided", file=sys.stderr)
            return None
