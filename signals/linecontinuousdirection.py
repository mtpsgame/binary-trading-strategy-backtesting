from enums import Direction
from signals.signal import Signal


class LineContinuousDirection(Signal):
    """
    Line continuous direction.
    """

    def __init__(self, delay=0, opposite=False, min_data_points_same_direction=1):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.
        :param min_data_points_same_direction: The minimum number of data points that the line must be moving in one
        constant direction (either up or down) before generating signals.
        """
        super(LineContinuousDirection, self).__init__(delay=delay, opposite=opposite)
        self._min_data_points_same_direction = min_data_points_same_direction

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(LineContinuousDirection, self).calculate_signal_positions(indicator_values)

        # Figure out the starting point of when a signal can occur.
        data_start_point = self.get_data_start_point(indicator_values)

        # If there is enough data for there to potentially be a direction the line moves in then...
        if data_start_point != -1 and len(indicator_values) > data_start_point + 1:

            # Is the line pointing up or down at the start.
            if indicator_values[data_start_point] < indicator_values[data_start_point + 1]:
                current_dir = Direction.UP
            else:
                current_dir = Direction.DOWN

            # Keep a track of when a change in direction last occurred.
            num_data_points_in_same_direction = 0
            for i in range(data_start_point + 1, len(indicator_values)):

                if current_dir == Direction.UP:

                    # If the line is continuing in the same direction, track its progress.
                    if indicator_values[i] > indicator_values[i - 1]:
                        num_data_points_in_same_direction += 1

                    # Reset the progress if the line changes direction.
                    elif indicator_values[i] < indicator_values[i - 1]:
                        num_data_points_in_same_direction = 1
                        current_dir = Direction.DOWN
                    else:
                        num_data_points_in_same_direction = 0
                else:
                    # If the line is continuing in the same direction, track its progress.
                    if indicator_values[i] < indicator_values[i - 1]:
                        num_data_points_in_same_direction += 1

                    # Reset the progress if the line changes direction.
                    elif indicator_values[i] > indicator_values[i - 1]:
                        num_data_points_in_same_direction = 1
                        current_dir = Direction.UP
                    else:
                        num_data_points_in_same_direction = 0

                # Generate a signal if the line has been moving in the same direction long enough.
                if num_data_points_in_same_direction >= self._min_data_points_same_direction:
                    self.generate_signal(i, current_dir)

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return super(LineContinuousDirection, self).to_string() \
               + "MinDataPointsSameDirection={}".format(self._min_data_points_same_direction)
