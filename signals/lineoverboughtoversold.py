from enums import Direction
from signals import Signal


class LineOverboughtOversold(Signal):
    """
    Line overbought / oversold.
    """

    def __init__(self, overbought_value, oversold_value, delay=0, opposite=False, signal_on_enter=True,
                 signal_on_exit=False, continuous_signal=False):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.:param overbought_value: The value of the overbought line.
        :param oversold_value: The value of the oversold line.
        :param signal_on_enter: If True, a signal will be generated as the line enters the over bought/sold zone.
        :param signal_on_exit: If True, a signal will be generated as the line leaves the over bought/sold zone.
        :param continuous_signal: Generate a signal at all points when the line is in the over bought/sold zone.
        """
        super(LineOverboughtOversold, self).__init__(delay=delay, opposite=opposite)
        self._overbought_value = overbought_value
        self._oversold_value = oversold_value
        self._signal_on_enter = signal_on_enter
        self._signal_on_exit = signal_on_exit
        self._continuous_signal = continuous_signal

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(LineOverboughtOversold, self).calculate_signal_positions(indicator_values)

        # Figure out the starting point of when a zone entry can occur.
        data_start_point = self.get_data_start_point(indicator_values)

        # If there is enough data for there to potentially be a zone entry then...
        if data_start_point != -1 and len(indicator_values) > data_start_point + 1:

            # If we only care about if the line is in the zone or not then...
            if self._continuous_signal:
                for i in range(data_start_point, len(indicator_values)):

                    # If the line is in the overbought region then...
                    if indicator_values[i] > self._overbought_value:
                        self.generate_signal(i, Direction.DOWN)

                    # If the line is in the oversold region then...
                    elif indicator_values[i] < self._oversold_value:
                        self.generate_signal(i, Direction.UP)

            # If not a continuous signal, need to be more detailed.
            else:

                # Track if the last data point was overbought or oversold, or neither.
                overbought = False
                oversold = False

                # Do we start overbought or oversold.
                if indicator_values[data_start_point] > self._overbought_value:
                    overbought = True
                elif indicator_values[data_start_point] < self._oversold_value:
                    oversold = True

                for i in range(data_start_point, len(indicator_values)):

                    # If the last data point was not overbought, but the current one is then...
                    if not overbought and indicator_values[i] >= self._overbought_value:
                        overbought = True

                        # If a signal should be generated for this then...
                        if self._signal_on_enter:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.DOWN)

                    # If the last data point was overbought, but the current one isn't then...
                    elif overbought and indicator_values[i] < self._overbought_value:
                        overbought = False

                        # If a signal should be generated for this then...
                        if self._signal_on_exit:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.DOWN)

                    # If the last data point was not overbought, but the current one is then...
                    if not oversold and indicator_values[i] <= self._oversold_value:
                        oversold = True

                        # If a signal should be generated for this then...
                        if self._signal_on_enter:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.UP)

                    # If the last data point was overbought, but the current one isn't then...
                    elif oversold and indicator_values[i] > self._oversold_value:
                        oversold = False

                        # If a signal should be generated for this then...
                        if self._signal_on_exit:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.UP)

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return super(LineOverboughtOversold, self).to_string() + "OverboughtValue={}, OversoldValue={}, " \
                                                                 "SignalOnEnter={}, SignalOnExit={}, " \
                                                                 "ContinuousSignal={}".format(
            self._overbought_value,
            self._oversold_value, self._signal_on_enter, self._signal_on_exit, self._continuous_signal)
