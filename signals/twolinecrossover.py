from enums import Relative, Direction
from signals import Signal


class TwoLineCrossover(Signal):
    """
    Two line crossover.
    """

    def __init__(self, delay=0, opposite=False, min_data_points_between_crossovers=0):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.
        :param min_data_points_between_crossovers: The minimum number of line data points since the last crossover
        before detecting a signal.
        """
        super(TwoLineCrossover, self).__init__(delay=delay, opposite=opposite)
        self._min_data_points_between_crossovers = min_data_points_between_crossovers

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(TwoLineCrossover, self).calculate_signal_positions(indicator_values)

        # Un-package the provided indicator data.
        line_one_values = indicator_values[0]
        line_two_values = indicator_values[1]

        # Figure out the starting point of when crossovers can occur.
        data_start_point_line_one = self.get_data_start_point(line_one_values)
        data_start_point_line_two = self.get_data_start_point(line_two_values)
        data_start_point = max(data_start_point_line_one, data_start_point_line_two)

        # If there is enough data for there to potentially be a crossover then...
        if data_start_point != -1 and len(line_one_values) > data_start_point:

            # Is line one above or below line two at the start.
            if line_two_values[data_start_point] < line_one_values[data_start_point]:
                line_one_relative_to_line_two = Relative.ABOVE
            else:
                line_one_relative_to_line_two = Relative.BELOW

            # Keep a track of where line one was relative to line two on the previous candle, and when the last
            # crossover occurred.
            last_crossover_index = data_start_point
            last_relative_position = line_one_relative_to_line_two
            for i in range(data_start_point, len(line_one_values)):

                # Track where line one is relative to line two on the current candle.
                if line_two_values[i] < line_one_values[i]:
                    line_one_relative_to_line_two = Relative.ABOVE
                elif line_two_values[i] > line_one_values[i]:
                    line_one_relative_to_line_two = Relative.BELOW

                # If the positions of the two lines relative to each other have changed from the previous candle to
                # this candle then...
                if last_relative_position != line_one_relative_to_line_two:

                    # If a crossover occurred longer ago than the minimum threshold then...
                    if i - last_crossover_index > self._min_data_points_between_crossovers:

                        # Add in a signal at this index.
                        if line_one_relative_to_line_two == Relative.ABOVE:
                            self.generate_signal(i, Direction.UP)
                        else:
                            self.generate_signal(i, Direction.DOWN)

                    # Update the most recent crossover to now.
                    last_crossover_index = i

                # Update last position ready for next iteration of the loop.
                last_relative_position = line_one_relative_to_line_two

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return super(TwoLineCrossover, self).to_string() + \
               "MinDataPointsBetweenCrossovers={}".format(self._min_data_points_between_crossovers)
