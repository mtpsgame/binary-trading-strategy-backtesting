import numpy as np
import pandas

from enums import Direction
from signals import Signal


class SignalContainer(Signal):
    """
    A container for signals, where each signal is combined using an 'AND' or 'OR' operator for a single indicator.
    """

    def __init__(self, signals, use_and_operator=True):
        """
        Initialisation.
        :param signals: List of signal objects that make up the container.
        :param use_and_operator: If True, a signal will only be generated at a given position by the container if all
        the signal objects generate the same signal at that position.
        """
        super(SignalContainer, self).__init__()
        self._signals = signals
        self._use_and_operator = use_and_operator

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(SignalContainer, self).calculate_signal_positions(indicator_values)

        for x in self._signals:
            x.calculate_signal_positions(indicator_values)

        signal_list = [x.get_signal_positions() for x in self._signals]

        for i in range(len(signal_list[0])):

            signals_at_i = [sig[i] for sig in signal_list]

            if self._use_and_operator:
                if np.nan not in signals_at_i:
                    if Direction.UP not in signals_at_i:
                        self.generate_signal(i, Direction.DOWN)
                    elif Direction.DOWN not in signals_at_i:
                        self.generate_signal(i, Direction.UP)
            else:
                for sig in signals_at_i:
                    if not pandas.isnull(sig):
                        self.generate_signal(i, sig)
                        break

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        string = super(SignalContainer, self).to_string() + " ["
        for x in self._signals:
            string += x.to_string() + ", "
        string += "]"
        return string
