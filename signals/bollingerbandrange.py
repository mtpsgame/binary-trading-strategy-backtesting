from enums import Direction
from signals import Signal


class BollingerBandRange(Signal):
    """
    Bollinger band range.
    """

    def __init__(self, delay=0, opposite=False, signal_on_exit=True, signal_on_enter=False, continuous_signal=False):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.
        :param signal_on_exit: If True, a signal will be generated as the close price leaves the bollinger band range.
        :param signal_on_enter: If True, a signal will be generated as the close price enters the bollinger band range.
        :param continuous_signal: Generate a signal at all points when the line is outside the bollinger band range.
        """
        super(BollingerBandRange, self).__init__(delay=delay, opposite=opposite)
        self._signal_on_exit = signal_on_exit
        self._signal_on_enter = signal_on_enter
        self._continuous_signal = continuous_signal

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(BollingerBandRange, self).calculate_signal_positions(indicator_values)

        # Un-package the provided indicator data.
        upper_band_values = indicator_values[0]
        lower_band_values = indicator_values[1]

        # Figure out the starting point of when a zone entry can occur.
        data_start_point_upper_band = self.get_data_start_point(upper_band_values)
        data_start_point_lower_band = self.get_data_start_point(lower_band_values)
        data_start_point = max(data_start_point_upper_band, data_start_point_lower_band)

        # If there is enough data for there to potentially be a zone entry then...
        if data_start_point != -1 and len(upper_band_values) > data_start_point + 1:

            # If we only care about if the line is in the range or not then...
            if self._continuous_signal:
                for i in range(data_start_point, len(upper_band_values)):

                    # If the line is above the upper band then...
                    if self._closing_prices[i] > upper_band_values[i]:
                        self.generate_signal(i, Direction.DOWN)

                    # If the line is below the lower band then...
                    elif self._closing_prices[i] < lower_band_values[i]:
                        self.generate_signal(i, Direction.UP)

            # If not a continuous signal, need to be more detailed.
            else:

                # Track if the last data point was above the upper band, below the lower band, or neither.
                above_upper_band = False
                below_lower_band = False

                # Do we start above the upper band or below the lower band.
                if self._closing_prices[data_start_point] > upper_band_values[data_start_point]:
                    above_upper_band = True
                elif self._closing_prices[data_start_point] < lower_band_values[data_start_point]:
                    below_lower_band = True

                for i in range(data_start_point, len(upper_band_values)):

                    # If the last data point was not above the upper band, but the current one is then...
                    if not above_upper_band and self._closing_prices[i] >= upper_band_values[i]:
                        above_upper_band = True

                        # If a signal should be generated for this then...
                        if self._signal_on_exit:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.DOWN)

                    # If the last data point was overbought, but the current one isn't then...
                    elif above_upper_band and self._closing_prices[i] < upper_band_values[i]:
                        above_upper_band = False

                        # If a signal should be generated for this then...
                        if self._signal_on_enter:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.DOWN)

                    # If the last data point was not below the upper band, but the current one is then...
                    if not below_lower_band and self._closing_prices[i] <= lower_band_values[i]:
                        below_lower_band = True

                        # If a signal should be generated for this then...
                        if self._signal_on_exit:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.UP)

                    # If the last data point was overbought, but the current one isn't then...
                    elif below_lower_band and self._closing_prices[i] > lower_band_values[i]:
                        below_lower_band = False

                        # If a signal should be generated for this then...
                        if self._signal_on_enter:
                            # Add in a signal at this index.
                            self.generate_signal(i, Direction.UP)

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return super(BollingerBandRange, self).to_string() + "SignalOnExit={}, SignalOnEnter={}, ContinuousSignal={}" \
            .format(self._signal_on_exit, self._signal_on_enter, self._continuous_signal)
