from enums import Direction
from signals.signal import Signal


class LineFlipDirection(Signal):
    """
    Line flip direction.
    """

    def __init__(self, delay=0, opposite=False, min_data_points_between_flips=0):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.:param min_data_points_between_flips: The minimum number of indicator data points since the last flip in
        direction before detecting a signal.
        """
        super(LineFlipDirection, self).__init__(delay=delay, opposite=opposite)
        self._min_data_points_between_flips = min_data_points_between_flips

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(LineFlipDirection, self).calculate_signal_positions(indicator_values)

        # Figure out the starting point of when a flip in direction can occur.
        data_start_point = self.get_data_start_point(indicator_values)

        # If there is enough data for there to potentially be a flip then...
        if data_start_point != -1 and len(indicator_values) > data_start_point + 1:

            # Is the line pointing up or down at the start.
            if indicator_values[data_start_point] < indicator_values[data_start_point + 1]:
                current_dir = Direction.UP
            else:
                current_dir = Direction.DOWN

            # Keep a track of which direction the ma was pointing in the previous candle and when the most recent flip
            # occurred.
            last_flip_index = data_start_point
            last_dir = current_dir
            for i in range(data_start_point + 1, len(indicator_values)):

                # Track which direction the ma is pointing on the current candle.
                if current_dir == Direction.UP and indicator_values[i] < indicator_values[i - 1]:
                    current_dir = Direction.DOWN
                elif current_dir == Direction.DOWN and indicator_values[i] > indicator_values[i - 1]:
                    current_dir = Direction.UP

                # If the direction the ma is pointing in has changed since the previous candle then...
                if last_dir != current_dir:

                    # If a flip occurred longer ago than the minimum threshold then...
                    if i - last_flip_index > self._min_data_points_between_flips:
                        # Add in a signal at this index.
                        self.generate_signal(i, current_dir)

                    # Update the most recent flip to now.
                    last_flip_index = i

                # Update last direction ready for next iteration of the loop.
                last_dir = current_dir

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return super(LineFlipDirection, self).to_string() \
               + "MinDataPointsBetweenFlips={}".format(self._min_data_points_between_flips)
