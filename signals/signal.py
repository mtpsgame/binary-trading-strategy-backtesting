from abc import ABCMeta, abstractmethod

import numpy as np

from enums import Direction


class Signal(object):
    """
    The base class for all signals to inherit.
    """
    __metaclass__ = ABCMeta

    def __init__(self, delay=0, opposite=False):
        """
        Initialisation.
        :param delay: How many candles should the signal be pushed back by.
        :param opposite: If true the generated signal points in the opposite direction to what it would do by default.
        """
        self._signal_positions = []
        self._delay = delay
        self._opposite = opposite
        self._closing_prices = []

    @abstractmethod
    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        if type(indicator_values[0]) in [list, tuple]:
            length = len(indicator_values[0])
        else:
            length = len(indicator_values)
        self._signal_positions = [np.nan] * length

    def set_closing_prices(self, closing_prices):
        """
        Sets the closing prices of the candles that make up the backtest data.
        :param closing_prices: The list of closing prices.
        """
        self._closing_prices = closing_prices

    def generate_signal(self, current_position, direction):
        """
        Generates a signal by adding an entry to the signal positions list.
        :param current_position: The position the signal was calculated at (not taking into account delay).
        :param direction: The direction of the signal.
        """
        signal_direction = direction
        if self._opposite:
            if signal_direction == Direction.UP:
                signal_direction = Direction.DOWN
            else:
                signal_direction = Direction.UP
        try:
            self._signal_positions[current_position + self._delay] = signal_direction
        except IndexError:
            pass

    @staticmethod
    def get_data_start_point(values):
        """
        Calculates the index of the first data point. Passed in indicator values will start with leading np.nan's.
        :param values: The list to find the starting point in.
        :return: The index of the first data point.
        """
        for i in range(len(values)):
            if not np.isnan(values[i]):
                return i
        return -1

    def get_signal_positions(self):
        """
        Gets the list of signal positions, consisting of NaN where there are no signals and a direction where there is.
        :return: The list of signal positions.
        """
        return self._signal_positions

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return self.__class__.__name__ + ": Delay={}, Opposite={}, ".format(self._delay, self._opposite)
