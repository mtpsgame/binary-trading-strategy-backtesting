from signals import Signal


class NoSignal(Signal):

    def __init__(self):
        """
        Initialisation.
        """
        super(NoSignal, self).__init__()

    def calculate_signal_positions(self, indicator_values):
        """
        Generates a list of elements, one for each indicator value, where each element is either NaN if there in no
        signal, or a direction (up or down).
        In this case, no signals will be generated.
        :param indicator_values: The indicator values used to calculate signal positions.
        """
        super(NoSignal, self).calculate_signal_positions(indicator_values)

    def to_string(self):
        """
        Gets a string representation of this signal.
        :return: String representation of object.
        """
        return "NA"
