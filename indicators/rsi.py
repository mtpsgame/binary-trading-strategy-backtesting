import numpy as np

from indicators import Indicator


class Rsi(Indicator):
    """
    Relative Strength Index.
    """

    def __init__(self, signal, period=14):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param period: The number of candles used in calculating the rsi.
        """
        super(Rsi, self).__init__(signal)
        self._period = period

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(Rsi, self).get_candles_needed_for_first_point()
        return self._period + 1

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the relative strength index of the data provided, as a list of data points.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(Rsi, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of closing prices.
        if not pure_data:
            data = dataset.get_closes()
        else:
            data = dataset

        # There are no data points to begin with, but make _indicator_values the same size as data.
        self._indicator_values = [np.nan] * (self.get_candles_needed_for_first_point() - 1)

        # The list containing the difference in closing price between every consecutive pair of candles.
        diffs = np.diff(data).tolist()

        # The initial average gain and loss, by looking over the last periods worth of price changes
        average_gain = sum([x for x in diffs[0:self._period] if x > 0]) / self._period
        average_loss = sum([-x for x in diffs[0:self._period] if x < 0]) / self._period

        # The ratio of gains to losses and RSI calculation.
        relative_strength = average_gain / average_loss
        rsi_value = 100 - (100 / (1 + relative_strength))

        self._indicator_values.append(rsi_value)

        count = 0
        for i in range(self.get_candles_needed_for_first_point(), len(data)):
            # The price change at the current data point, one of these two variables will be zero.
            current_gain = max(diffs[self._period + count], 0)
            current_loss = -min(0, diffs[self._period + count])

            # Calculation for the average gain and loss.
            average_gain = ((average_gain * (self._period - 1)) + current_gain) / self._period
            average_loss = ((average_loss * (self._period - 1)) + current_loss) / self._period

            # The ratio of gains to losses and RSI calculation.
            relative_strength = average_gain / average_loss
            rsi_value = 100 - (100 / (1 + relative_strength))

            count += 1
            self._indicator_values.append(rsi_value)

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(Rsi, self).to_string() + "Period={}".format(self._period)
