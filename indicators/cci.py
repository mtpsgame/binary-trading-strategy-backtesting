import numpy as np

from indicators import Indicator, Sma
from signals import NoSignal


class Cci(Indicator):
    """
    Commodity Channel Index.
    """

    def __init__(self, signal, period, cci_constant=0.015):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param period: The number of candles used in calculating the cci.
        :param cci_constant: A constant used to reduce control the typical range of values.
        """
        super(Cci, self).__init__(signal)
        self._period = period
        self._cci_constant = cci_constant

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(Cci, self).get_candles_needed_for_first_point()
        return self._period

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the commodity channel index for the data provided, as a list of data points.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(Cci, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of high, low and closing prices.
        if not pure_data:
            highs = dataset.get_highs()
            lows = dataset.get_lows()
            closes = dataset.get_closes()
        else:
            highs = dataset[0]
            lows = dataset[1]
            closes = dataset[2]

        # Calculate the typical price for every data point.
        typical_price = []
        for i in range(len(closes)):
            typical_price.append((highs[i] + lows[i] + closes[i]) / 3)

        # Calculate an sma of the typical prices.
        tp_averages = Sma(NoSignal(), self._period)
        tp_averages.calculate_indicator_data_points(typical_price, pure_data=True)
        tp_average_values = tp_averages.get_indicator_data_points()

        # Calculate the mean deviation (absolute average deviation) between the sma and typical price values.
        mean_deviation = [np.nan] * (self.get_candles_needed_for_first_point() - 1)
        for i in range(tp_averages.get_candles_needed_for_first_point() - 1, len(closes)):
            total = 0
            for j in range(self._period):
                total += abs(typical_price[i - j] - tp_average_values[i])
            mean_deviation.append(total / self._period)

        # There are no data points to begin with, but make _indicator_values the same size as data.
        self._indicator_values = [np.nan] * (self.get_candles_needed_for_first_point() - 1)

        # From all the data calculated above, now run the cci formula.
        for i in range(tp_averages.get_candles_needed_for_first_point() - 1, len(closes)):
            self._indicator_values.append(
                (typical_price[i] - tp_average_values[i]) / (self._cci_constant * mean_deviation[i]))

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(Cci, self).to_string() + "Period={}, CCIConstant={}".format(self._period, self._cci_constant)
