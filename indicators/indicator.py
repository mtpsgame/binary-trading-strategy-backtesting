from abc import ABCMeta, abstractmethod


class Indicator(object):
    """
    The base class for all indicators to inherit.
    """
    __metaclass__ = ABCMeta

    def __init__(self, signal):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        """
        self._indicator_values = []
        self._signal = signal

    @abstractmethod
    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        return 0

    @abstractmethod
    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        This function should take in the backtest data, and populate self.indicator_values with the results of applying
        this indicator.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        self._indicator_values = []
        if pure_data:
            self._signal.set_closing_prices(dataset)
        else:
            self._signal.set_closing_prices(dataset.get_closes())

    def calculate_signal_positions(self):
        """
        Provides the indicator values to the signal, and makes it calculate the position of any calls or puts.
        """
        self._signal.calculate_signal_positions(self.get_indicator_data_points())

    def get_indicator_data_points(self):
        """
        Gets the indicator data points.
        :return: Indicator values.
        """
        return self._indicator_values

    def get_signal_positions(self):
        """
        Gets the signal positions from the signal object associated with this indicator.
        :return: List of signal positions.
        """
        return self._signal.get_signal_positions()

    def get_signal(self):
        """
        Gets the signal object associated with this indicator.
        :return: Signal object.
        """
        return self._signal

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return self.__class__.__name__ + ": "
