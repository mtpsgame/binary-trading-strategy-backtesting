from statistics import pstdev

import numpy as np

from indicators import Sma
from indicators.indicator import Indicator
from signals import NoSignal


class BollingerBands(Indicator):
    """
    Bollinger Bands.
    """

    def __init__(self, signal, period=20, std_deviation=2):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param period: The number of candles used in calculating the moving average.
        :param std_deviation: The factor to multiply the standard deviation by.
        """
        super(BollingerBands, self).__init__(signal)
        self._period = period
        self._std_deviation = std_deviation

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(BollingerBands, self).get_candles_needed_for_first_point()
        return self._period

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the bollinger bands of the data provided, as a list of two lists. The first list is the upper band
        data points and the second is the lower band data points.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(BollingerBands, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of closing prices.
        if not pure_data:
            data = dataset.get_closes()
        else:
            data = dataset

        moving_average = Sma(NoSignal(), self._period)
        moving_average.calculate_indicator_data_points(dataset, pure_data=pure_data)
        moving_average_values = moving_average.get_indicator_data_points()

        # There are no data points to begin with, but make _indicator_values the same size as data.
        start_point = self.get_candles_needed_for_first_point() - 1
        upper_band = [np.nan] * start_point
        lower_band = [np.nan] * start_point

        for i in range(self.get_candles_needed_for_first_point() - 1, len(data)):
            standard_deviation = pstdev(data[i - self._period + 1:i + 1])
            upper_band.append(moving_average_values[i] + (standard_deviation * self._std_deviation))
            lower_band.append(moving_average_values[i] - (standard_deviation * self._std_deviation))

        self._indicator_values = [upper_band, lower_band]

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(BollingerBands, self).to_string() + \
               "Period={}, StandardDeviation={}".format(self._period, self._std_deviation)
