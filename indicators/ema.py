import numpy as np

from indicators import Indicator


class Ema(Indicator):
    """
    Exponential Moving Average.
    """

    def __init__(self, signal, period):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param period: The number of candles used in calculating the moving average.
        """
        super(Ema, self).__init__(signal)
        self._period = period

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(Ema, self).get_candles_needed_for_first_point()
        return self._period

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the exponential moving average of the data provided, as a list of data points.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(Ema, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of closing prices.
        if not pure_data:
            data = dataset.get_closes()
        else:
            data = dataset

        # There are no data points to begin with, but make _indicator_values the same size as data.
        self._indicator_values = [np.nan] * (self.get_candles_needed_for_first_point() - 1)

        # The exponential moving average calculation, for each candle.
        starting_value = sum(data[0: self._period]) / self._period
        self._indicator_values.append(starting_value)
        k = 2 / (self._period + 1)
        for i in range(self._period - 1, len(data)):
            current_ema_value = (data[i] * k) + (self._indicator_values[-1] * (1 - k))
            self._indicator_values.append(current_ema_value)

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(Ema, self).to_string() + "Period={}".format(self._period)
