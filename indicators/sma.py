import numpy as np

from indicators import Indicator


class Sma(Indicator):
    """
    Simple Moving Average.
    """

    def __init__(self, signal, period):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param period: The number of candles used in calculating the moving average.
        """
        super(Sma, self).__init__(signal)
        self._period = period

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(Sma, self).get_candles_needed_for_first_point()
        return self._period

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the simple moving average of the data provided, as a list of data points.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(Sma, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of closing prices.
        if not pure_data:
            data = dataset.get_closes()
        else:
            data = dataset

        # There are no data points to begin with, but make _indicator_values the same size as data.
        self._indicator_values = [np.nan] * (self.get_candles_needed_for_first_point() - 1)

        # The simple moving average calculation, for each candle.
        for i in range(self.get_candles_needed_for_first_point() - 1, len(data)):
            current_sma_value = sum(data[i - self._period + 1:i + 1]) / self._period
            self._indicator_values.append(current_sma_value)

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(Sma, self).to_string() + "Period={}".format(self._period)
