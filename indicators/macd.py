import numpy as np

from indicators import Indicator, Ema
from signals import NoSignal


class Macd(Indicator):
    """
    Moving Average Convergence Divergence.
    """

    def __init__(self, signal, ema_a_period=12, ema_b_period=26, signal_period=9):
        """
        Initialisation.
        :param signal: The signal object that will generate calls and puts.
        :param ema_a_period: The smaller period ema line used the calculate the macd line.
        :param ema_b_period: The larger period ema line used the calculate the macd line.
        :param signal_period: The signal (ema) line period.
        """
        super(Macd, self).__init__(signal)
        self._ema_a_period = ema_a_period
        self._ema_b_period = ema_b_period
        self._signal_period = signal_period

    def get_candles_needed_for_first_point(self):
        """
        Gets the number of candles needed until this indicator can produce its first value.
        :return: The number of candles.
        """
        super(Macd, self).get_candles_needed_for_first_point()
        return self._ema_b_period + self._signal_period

    def calculate_indicator_data_points(self, dataset, pure_data=False):
        """
        Calculates the moving average convergence divergence of the data provided, as two list of data points, the
        first being the macd line, and second being the signal line.
        :param dataset: The dataset object that contains the data points.
        :param pure_data: True if dataset is a list of data points instead of a dataset instance.
        """
        super(Macd, self).calculate_indicator_data_points(dataset, pure_data)

        # Get the list of closing prices.
        if not pure_data:
            data = dataset.get_closes()
        else:
            data = dataset

        # Calculate the building blocks of the macd line using the base ema indicator.
        ema_a_line = Ema(NoSignal(), self._ema_a_period)
        ema_b_line = Ema(NoSignal(), self._ema_b_period)
        ema_a_line.calculate_indicator_data_points(dataset, pure_data=pure_data)
        ema_b_line.calculate_indicator_data_points(dataset, pure_data=pure_data)
        ema_a_values = ema_a_line.get_indicator_data_points()
        ema_b_values = ema_b_line.get_indicator_data_points()

        # There are no data points to begin with, but make _macd_line_values the same size as data.
        macd_line_start_point = ema_b_line.get_candles_needed_for_first_point() - 1
        macd_line_values = [np.nan] * macd_line_start_point

        # Calculate the macd line.
        macd_line_values += [ema_a_values[i] - ema_b_values[i] for i in range(macd_line_start_point, len(data))]

        # There are no data points to begin with, but make _signal_line_values the same size as data.
        signal_line_values = [np.nan] * macd_line_start_point

        # Calculate the signal line.
        signal_line = Ema(NoSignal(), self._signal_period)
        signal_line.calculate_indicator_data_points(macd_line_values[macd_line_start_point:], pure_data=True)
        signal_line_values += signal_line.get_indicator_data_points()

        self._indicator_values = [macd_line_values, signal_line_values]

    def to_string(self):
        """
        Gets a string representation of this indicator.
        :return: String representation of object.
        """
        return super(Macd, self).to_string() + "EmaAPeriod={}, EmaBPeriod={}, SignalPeriod={}" \
            .format(self._ema_a_period, self._ema_b_period, self._signal_period)
