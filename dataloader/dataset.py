from datetime import datetime


class Dataset(object):
    """
    Contains a consecutive series of price data points.
    """

    def __init__(self, data, date_format):
        """
        Initialisation.
        :param data: A list of elements, where each element is in the format [date, open, high, low, close] and every
        value is a string.
        :param date_format: A string representation of the date column.
        """
        self._size = len(data)
        self._dates = [datetime.strptime(x[0], date_format) for x in data]
        self._opens = [float(x[1]) for x in data]
        self._highs = [float(x[2]) for x in data]
        self._lows = [float(x[3]) for x in data]
        self._closes = [float(x[4]) for x in data]

    def get_number_of_data_points(self):
        """
        Gets the number of data points in this dataset.
        :return: Number of data points.
        """
        return self._size

    def get_dates(self):
        """
        Gets each date time in the dataset.
        :return: List of date times.
        """
        return self._dates

    def get_opens(self):
        """
        Gets each open price in the dataset.
        :return: List of open prices.
        """
        return self._opens

    def get_highs(self):
        """
        Gets each high price in the dataset.
        :return: List of high prices.
        """
        return self._highs

    def get_lows(self):
        """
        Gets each low price in the dataset.
        :return: List of low prices.
        """
        return self._lows

    def get_closes(self):
        """
        Gets each close price in the dataset.
        :return: List of close prices.
        """
        return self._closes
