import os
from copy import copy
from datetime import time, datetime, timedelta, MINYEAR

import project_root
from dataloader import Dataset


class Loader(object):
    """
    Utility designed to load the backtest data from a csv file into a collection of datasets, one for each day.
    """

    def __init__(self, params):
        """
        Initialisation.
        Sets the days and times of interest. Any price data outside the range of interest are dropped.
        :param params: A dictionary holding the required parameters.
        """
        self._params = params
        self._data_path = os.path.join(project_root.root_directory, self._params["data_path"])
        self._date_format = self._params["date_format"]
        self._has_loaded_raw_data = False
        self._raw_data = []
        self._raw_dates = []
        self._days_to_load = []

        days_of_interest = self._params["days_to_load"]
        if "Monday" in days_of_interest:
            self._days_to_load.append(0)
        if "Tuesday" in days_of_interest:
            self._days_to_load.append(1)
        if "Wednesday" in days_of_interest:
            self._days_to_load.append(2)
        if "Thursday" in days_of_interest:
            self._days_to_load.append(3)
        if "Friday" in days_of_interest:
            self._days_to_load.append(4)
        if "Saturday" in days_of_interest:
            self._days_to_load.append(5)
        if "Sunday" in days_of_interest:
            self._days_to_load.append(6)

        times_of_interest = self._params["times_to_load"]
        self._start_hour = times_of_interest["start_hour"]
        self._start_min = times_of_interest["start_min"]
        self._end_hour = times_of_interest["end_hour"]
        self._end_min = times_of_interest["end_min"]

        self._datasets = []

    def load_data(self, max_candles_needed_for_first_point):
        """
        Loads the data from the csv file specified in params and converts that into a collection of dataset objects,
        one for each day of the data.
        If this function is called more than once, the cached csv file contents are used as apposed to reading the csv
        file from scratch again.
        """
        if not self._has_loaded_raw_data:

            # Loads the file into a list of tuples, each tuple in the following format
            # (date, open, high, low, close)
            # Each element in each tuple is a string at this stage.
            # Elements which are on days not required by the backtest are not loaded.
            self._raw_data = []
            column_numbers = self._params["columns"]
            with open(self._data_path, 'r') as data_file:
                data_file_lines = data_file.readlines()
                for i in range(len(data_file_lines)):
                    if i < self._params["start_row"] or data_file_lines[i].startswith(self._params["comment_symbol"]):
                        continue
                    line_text = data_file_lines[i].split(self._params["delimiter"])
                    date_of_line = datetime.strptime(line_text[column_numbers["date"]], self._date_format)
                    if date_of_line.weekday() in self._days_to_load:
                        self._raw_data.append((line_text[column_numbers["date"]], line_text[column_numbers["open"]],
                                               line_text[column_numbers["high"]], line_text[column_numbers["low"]],
                                               line_text[column_numbers["close"]]))
                        self._raw_dates.append(date_of_line)

            self._has_loaded_raw_data = True

        data = []

        # Removes any elements from data where the time is not in the required range.
        start_time = (datetime(1, 1, MINYEAR, hour=self._start_hour, minute=self._start_min) -
                      timedelta(minutes=max_candles_needed_for_first_point)).time()
        end_time = time(self._end_hour, self._end_min, 0)
        for i in range(len(self._raw_data)):
            if start_time <= self._raw_dates[i].time() < end_time:
                data.append(copy(self._raw_data[i]))

        # Splits the data up into datasets, one for each individual day.
        self._datasets = []
        dates = [datetime.strptime(x[0], self._date_format) for x in data]
        start_of_current_day = 0
        current_day = dates[0].day
        for i in range(len(data)):
            if dates[i].day != current_day:
                self._datasets.append(Dataset(data[start_of_current_day:i], self._date_format))
                current_day = dates[i].day
                start_of_current_day = i
        self._datasets.append(Dataset(data[start_of_current_day:], self._date_format))

    def get_datasets(self):
        """
        Gets the list of dataset objects populated when load data is called.
        :return: List of dataset objects.
        """
        return self._datasets

    def get_start_time(self):
        """
        Gets the start time of day that is of interest.
        :return: Start time as a time object.
        """
        return time(self._start_hour, self._start_min, 0)

    def get_end_time(self):
        """
        Gets the end time of day that is of interest.
        :return: End time as a time object.
        """
        return time(self._end_hour, self._end_min, 0)

    def get_days(self):
        """
        Gets the days of interest as a list, where 0 = Monday, 1 = Tuesday, ..., 6 = Sunday.
        :return: List of days as integers.
        """
        return self._days_to_load

    def get_dates(self):
        """
        Gets the dates that have been loaded into dataset objects.
        :return: List of dates as date objects.
        """
        return [x.date() for x in self._raw_dates]
