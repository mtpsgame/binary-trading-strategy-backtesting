import numpy as np

from enums import Outcome


class BacktestResult(object):
    """
    Holds the resulting timeline of events from a backtest simulation.
    """

    def __init__(self, timeline):
        """
        Initialisation
        :param timeline: The timeline of events from a backtest simulation, in the form of a list of 3-tuples. Each
        tuple is in the form (enums.Option, enums.Outcome, closing_price_at_entry) if there was a signal generated,
        or (np.nan, np.nan, np.nan) if no signal was generated.
        """
        self._timeline = timeline

    def get_timeline(self):
        """
        Gets the timeline of this result.
        :return: The timeline object.
        """
        return self._timeline

    def get_timeline_specific_result(self, result):
        """
        Gets the timeline but only including signals that resulted in a certain outcome.
        :param result: The result of interest as an enum.Outcome.
        :return: The timeline object.
        """
        list_to_return = []
        for x in self._timeline:
            if np.isnan(x[2]):
                list_to_return.append(x)
            else:
                if x[1] == result:
                    list_to_return.append(x)
                else:
                    list_to_return.append((np.nan, np.nan, np.nan))
        return list_to_return

    def get_number_of_specific_result(self, result):
        """
        Gets the number of times a specific outcome occurred.
        :param result: The result of interest as an enum.Outcome.
        :return: Number of times outcome occurred.
        """
        return len([x for x in self._timeline if x[1] == result])

    def get_win_rate(self):
        """
        Gets the percentage of profitable trades out of all trades made.
        :return: Percentage of profitable trades.
        """
        if self.get_number_of_specific_result(Outcome.PROFIT) == 0:
            return 0
        return self.get_number_of_specific_result(Outcome.PROFIT) / (self.get_number_of_specific_result(Outcome.PROFIT)
                                                                     + self.get_number_of_specific_result(Outcome.LOSS))
