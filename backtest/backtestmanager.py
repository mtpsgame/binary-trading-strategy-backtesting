from copy import deepcopy
from multiprocessing import Pool
from time import time

from backtest import Backtest, BacktestResult
from dataloader import Loader
from logger import Logger
from params import ParamsReader


class BacktestManager(object):
    """
    Starting point of backtesting.
    Handles creating the dataloader and splitting loaded data into individual backtests. Concurrently executes backtests
    and then collates the results.
    """

    def __init__(self, logger_type=Logger, loader_type=Loader):
        """
        Initialisation.
        :param logger_type: The type of logger to use to store the results of backtest simulations.
        :param loader_type: The type of loader to use to load the data into datasets.
        """
        self._result = None
        self._backtests = []
        self._indicators_to_test = []
        self._params = ParamsReader()
        self._logger = logger_type(self._params.get_value("logger", "log_path"))
        self._dataloader = loader_type(self._params.get_value("loader"))

    def reset(self):
        """
        Removes all indicators.
        """
        self._result = None
        self._backtests = []
        self._indicators_to_test = []

    def add_indicator(self, indicator):
        """
        Adds an indicator to the collection of indicators in the backtest simulation.
        :param indicator: The indicator to add.
        """
        self._indicators_to_test.append(indicator)

    def run(self, option_duration=1, log_result=True):
        """
        Runs the backtesting algorithm on the data supplied by the loader instance. Concurrently runs a backtest
        simulation on each dataset in provided by the loader.
        :param option_duration: How many candles long should the expiry of each option be.
        :param log_result: If True then the result will be passed to the logger to write to the log file.
        """
        print("Loading data")
        start_time = time()

        self._dataloader.load_data(max([x.get_candles_needed_for_first_point() for x in self._indicators_to_test]))

        print("Time taken: {} seconds\n".format(time() - start_time))
        print("Starting backtest")
        start_time = time()

        workers = Pool(processes=self._params.get_value("simulation", "threads"))
        self._backtests = [Backtest() for i in range(len(self._dataloader.get_datasets()))]

        for test in self._backtests:
            for indicator in self._indicators_to_test:
                test.add_indicator(deepcopy(indicator))

        results = []
        for i in range(len(self._dataloader.get_datasets())):
            backtest_object = self._backtests[i]
            dataset_object = self._dataloader.get_datasets()[i]
            results.append(
                workers.apply_async(_run_single_backtest, args=(backtest_object, dataset_object, option_duration,)))

        self._backtests.clear()

        for res in results:
            self._backtests.append(res.get())

        workers.close()
        workers.join()

        self._backtests.sort(key=lambda b: b.get_dataset().get_dates()[0])
        combined_timeline = []
        for backtest in self._backtests:
            combined_timeline.extend(backtest.get_result().get_timeline())
        self._result = BacktestResult(combined_timeline)

        print("Time taken: {} seconds\n".format(time() - start_time))

        if log_result:
            self._logger.log_result(self._indicators_to_test, self._backtests, self._result, self._params)

    def get_result(self):
        """
        Gets the results of the backtest simulation as a backtest results object.
        :return: The result object.
        """
        return self._result


def _run_single_backtest(backtest, dataset, option_duration):
    """
    Runs a single backtest simulation on the provided backtest object.
    :param backtest: The backtest object that runs the simulation.
    :param dataset: The data to simulate.
    :param option_duration: How many candles long should the expiry of each option be.
    :return: The backtest object after the simulation is complete.
    """
    backtest.run(dataset, option_duration)
    return backtest
