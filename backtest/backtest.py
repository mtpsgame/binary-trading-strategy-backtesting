import numpy as np

from backtest import BacktestResult
from enums import Direction, Outcome, Trade


class Backtest(object):
    """
    Runs a backtesting simulation, given a collection of indicator-signal pairs.
    """

    def __init__(self):
        """
        Initialisation.
        """
        self._dataset = None
        self._result = None
        self._indicators_to_test = []

    def reset(self):
        """
        Removes all indicators and simulations.
        """
        self._dataset = None
        self._result = None
        self._indicators_to_test = []

    def add_indicator(self, indicator):
        """
        Adds an indicator to the collection of indicators in the backtest simulation.
        :param indicator: The indicator to add.
        """
        self._indicators_to_test.append(indicator)

    def run(self, dataset, option_duration):
        """
        Runs the backtest simulation. Calculates how profitable the specified strategy would have been on the given test
        data. It will iterate through all of the indicators, calling them to calculate their values and signal points,
        then simulate an option (call or put) when all indicators provide a unanimous signal.
        :param dataset: The dataset object to run the backtest on.
        :param option_duration: How many candles long should the expiry of each option be.
        """
        self._dataset = dataset
        timeline = [(np.nan, np.nan, np.nan)] * self._dataset.get_number_of_data_points()

        # Calls the appropriate indicator functions to calculate their values and signal points (and directions).
        for i in self._indicators_to_test:
            i.calculate_indicator_data_points(self._dataset)
            i.calculate_signal_positions()

        # Figures out from which part of the data can we start simulating calls and puts.
        max_candles_needed_for_first_point = max(
            [x.get_candles_needed_for_first_point() for x in self._indicators_to_test])

        # The option simulation loop. Loops though each candle from the earliest point possible where all indicators
        # are available.
        for i in range(max_candles_needed_for_first_point, self._dataset.get_number_of_data_points() - option_duration):
            # Gets the signal data for each indicator at the current candle point.
            signals_at_index_i = [x.get_signal_positions()[i] for x in self._indicators_to_test]

            # If a signal was generated at this point for every indicator then...
            if np.nan not in signals_at_index_i:

                # If all all the signals point down then... (simulate a put)
                if Direction.UP not in signals_at_index_i:

                    # Calculate the outcome of the option for the given expiry time.
                    if self._dataset.get_closes()[i] > self._dataset.get_closes()[i + option_duration]:
                        result = Outcome.PROFIT
                    elif self._dataset.get_closes()[i] < self._dataset.get_closes()[i + option_duration]:
                        result = Outcome.LOSS
                    else:
                        result = Outcome.DRAW
                    timeline[i] = (Trade.PUT, result, self._dataset.get_closes()[i])

                # If all all the signals point up then... (simulate a call)
                elif Direction.DOWN not in signals_at_index_i:

                    # Calculate the outcome of the option for the given expiry time.
                    if self._dataset.get_closes()[i] < self._dataset.get_closes()[i + option_duration]:
                        result = Outcome.PROFIT
                    elif self._dataset.get_closes()[i] > self._dataset.get_closes()[i + option_duration]:
                        result = Outcome.LOSS
                    else:
                        result = Outcome.DRAW
                    timeline[i] = (Trade.CALL, result, self._dataset.get_closes()[i])

        # Store the result of the backtest simulation.
        self._result = BacktestResult(timeline)

    def get_dataset(self):
        """
        Get the dataset object the backtest simulation was based on.
        :return: The dataset object.
        """
        return self._dataset

    def get_result(self):
        """
        Gets the results of the backtest simulation as a backtest results object.
        :return: The result object.
        """
        return self._result
