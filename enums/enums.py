from enum import Enum


class Direction(Enum):
    UP = 1
    DOWN = 2


class Trade(Enum):
    CALL = 1
    PUT = 2


class Outcome(Enum):
    PROFIT = 1
    LOSS = 2
    DRAW = 3


class Relative(Enum):
    ABOVE = 1
    BELOW = 2
    IN = 3
    OUT = 4
