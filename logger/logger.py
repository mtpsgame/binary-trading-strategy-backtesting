import os
import sys
from datetime import time

import project_root
from enums import Outcome


class Logger(object):
    """
    Logs text to a log file.
    """

    def __init__(self, filepath):
        """
        Initialisation
        :param filepath: The path to the logger file.
        """
        self._filepath = os.path.join(project_root.root_directory, filepath)
        if not os.path.exists(os.path.dirname(self._filepath)):
            try:
                os.makedirs(os.path.dirname(self._filepath))
            except OSError:
                print("Unable to create the directory / file " + self._filepath, file=sys.stderr)

    def log(self, text):
        """
        Logs text to the log file and starts a new line.
        :param text: The text to write to the log file.
        """
        file = open(self._filepath, 'a')
        file.write(text + "\n")
        file.close()

    def log_result(self, indicators, backtests, result, params):
        """
        Takes the result object and writes it in the log file.
        :param indicators: The list of indicators used in the backtest.
        :param backtests: The list of individual backtest objects.
        :param result: The backtest result object to log.
        :param params: The params reader.
        """
        log_text = "=========================="
        log_text += "\n\n"
        log_text += "DAYS: "
        log_text += str(params.get_value("loader", "days_to_load"))
        log_text += " from {} to {}".format(str(backtests[0].get_dataset().get_dates()[0].date()),
                                            str(backtests[-1].get_dataset().get_dates()[0].date()))
        log_text += "\n"
        log_text += "TIMES: "
        log_text += str(time(params.get_value("loader", "times_to_load", "start_hour"),
                             params.get_value("loader", "times_to_load", "start_min"))) + " - " + \
                    str(time(params.get_value("loader", "times_to_load", "end_hour"),
                             params.get_value("loader", "times_to_load", "end_min")))
        log_text += "\n\n"
        log_text += "INDICATORS AND SIGNALS:"
        log_text += "\n"
        for x in indicators:
            log_text += x.to_string()
            log_text += " -> "
            log_text += x.get_signal().to_string()
            log_text += "\n"
        log_text += "\n"
        log_text += "PERFORMANCE:"
        log_text += "\n"
        log_text += "WIN RATE="
        log_text += "{:.4%}".format(result.get_win_rate())
        log_text += "\n"
        log_text += "PROFITS="
        log_text += str(result.get_number_of_specific_result(Outcome.PROFIT))
        log_text += "\n"
        log_text += "LOSSES="
        log_text += str(result.get_number_of_specific_result(Outcome.LOSS))
        log_text += "\n"
        log_text += "DRAWS="
        log_text += str(result.get_number_of_specific_result(Outcome.DRAW))
        log_text += "\n"
        self.log(log_text)
