# Strategy Backtesting #

A python solution to backtesting a binary options strategy, using a range of indicators and signals.

### What indicators are included? ###

The currently supported indicators are as follows:

* Simple moving average _(SMA)_
* Exponential moving average _(EMA)_
* Moving average convergence divergence _(MACD)_
* Commodity Channel Index _(CCI)_
* Bollinger Bands _(BB)_

There are also composite indicators, which are some collections of the individual indicators listed above.

### How to backtest ###

##### 1) Configure the historical data #####

* Find historical price data in a csv format and ensure this includes the following columns:
    * Date _(YYYYMMDD)_
    * Time _(HHMMSS)_
    * Open
    * High
    * Low
    * Close

* Update the variables with the prefix `loader` in `constants.py` to specify how to read the csv file.

##### 2) Define a data loader object #####

* Create a `loader` instance. The constructor has many optional parameters to specify the time of day to test and which days of the week.

##### 3) Define a backtesting manager object #####

* Create a `backtest_manager` instance. The constructor requires the loader object created in the previous step.
* This is what will run the backtests and store the results.

##### 4) Create indicator and signal instances #####

* For each `indicator` you want to use, create an instance of it. They all take different optional parameters in the constructor to change their behaviour.
* Each indicator requires a `signal` instance to be passed to it via the constructor. These also have optional parameters that effect how the signals are generated.

##### 5) Supply the indicators and signals to the backtesting object #####

* For each indicator instance, pass it into the `backtest_manager.add_indicator` function on the backtesting manager object created earlier.

##### 6) Run the backtest #####

* Call the `backtest_manager.run` function on the backtesting manager object created earlier.
* This call will load the data into memory, run each indicator-signal pair on that data, and then calculate the profitability of the strategy.
* The call will take some time to return, however useful console messages will be displayed throughout the backtesting process.

##### 7) Get the results #####

* The results of the backtest will be logged to the logfile specified in `constants.py`.
* The `backtest_manager` object provides getters to retrieve the results programmatically.
